import { Component } from '@angular/core';
import { MyNewComponentComponent} from '././my-new-component/my-new-component.component';
import { trigger,state,style,transition,animate,keyframes } from '@angular/animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    trigger('myAwesomeAnimation', [
      state('small', style({
        transform: 'scale(1)'
      })),
      state('large', style({
        transform: 'scale(1.2)'
      })),

      transition('small <=> large', animate('300ms ease-in', keyframes([
        style({
          opacity: 0, transform: 'translateY(-75%)', offset: 0 
        }),
        style({
          opacity: 1, transform: 'translateY(35%)', offset: .5 
        }),
        style({
          opacity: 1, transform: 'translateY(0)', offset: 1
        })
      ]))),
    ])
  ]
})
export class AppComponent {
//   imageLink : string
//   titleClass = {
//     'red-title ': true,
//     'large-title' : true
//   }
//   constructor() {
//     this.imageLink = "http://lorempixel.com/400/200"
//   }
 
//   myArray : any[] = ["Angularjs", "ionic", "Java", "Nodejs", "Typescript"];
//   isTrue = true

// toggleVar() {
//   this.isTrue = false
// }
// dblToggle(){
//   this.isTrue = true;
// }

  state: string = 'small';

  animateMe() {
    this.state = (this.state === 'small' ? 'large' : 'small');
  }
}
