import { Component, OnInit } from '@angular/core';
import { DataService} from './../data.service';

@Component({
  selector: 'app-my-new-component',
  templateUrl: './my-new-component.component.html',
  styleUrls: ['./my-new-component.component.css']
})
export class MyNewComponentComponent implements OnInit {

  constructor(private dataservice : DataService) {
    
   }

   titleStyle = {
    'color' : 'red',
    'font-size':  '3em',
   };

   someMessage: string = '';
  ngOnInit() {
    console.log(this.dataservice.cars)
    
    this.someMessage = this.dataservice.doSomething();
  }
  

  doSomething(event):void {
    console.log(event)
  }
}
